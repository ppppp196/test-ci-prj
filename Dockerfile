FROM node:alpine

COPY ./public /home/node/app/
COPY server.js /home/node/app/
RUN ls /home/node/app/
CMD [ "node", "server.js" ]